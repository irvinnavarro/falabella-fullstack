package cl.config;

import cl.models.ErrorResponse;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import cl.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.http.HttpStatus;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserService userService;
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // DECLARANDO OBJETOS GENERALES
        HttpStatus httpStatus;
        // PARAMETROS DE LA PETICION
        final String jwtToken = request.getHeader("Authorization");
        // DECLARANDO E INICIANDO VARIABLES
        String httpUrl = request.getServletPath();
        String userName;
        try {
            // SI EL TOKEN ES RECIBIDO COMO PARAMETRO
            if (jwtToken != null && !jwtToken.equals("undefined")) {
                // SE OBTIENE EL USERNAME A PARTIR DEL TOKEN RECIBIDO
                userName = jwtTokenUtil.getUsernameFromToken(jwtToken);
                // SI EL USUARIO ES VALIDO Y AUN NO ESTA AUTENTICADO
                if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    // SE OBTIENE DETALLE DE USUARIO EN BASE DE DATOS
                    UserDetails userDetails = this.userService.loadUserByUsername(userName);
                    // SI TOKEN ES VALIDO PARA EL USUARIO DADO
                    if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
                        // SE CREA LA AUTENTICACION DEL USUARIO
                        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));        
                        // DESPUES DE CONFIGURAR LA AUTENTICACION, SE ESPECIFICA QUE EL USUARIO ACTUAL ESTA AUTENTICADO
                        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                    }
                }
            }
            // SI EL TOKEN NO RECIBIDO COMO PARAMETRO
            chain.doFilter(request, response);
        }
        // PARAMETRO AUTHORIZATION VACIO
        catch (IllegalArgumentException e) {
            httpStatus = HttpStatus.FORBIDDEN;
            handleException(response, httpStatus, e, httpUrl, "No se puede obtener el token JWT");
        }
        // PARAMETRO AUTHORIZATION CON FORMATO NO VALIDO. LA FIRMA JWT (jwt.secret) NO ES CORRECTA
        catch (MalformedJwtException | SignatureException e) {
            httpStatus = HttpStatus.BAD_REQUEST;
            handleException(response, httpStatus, e, httpUrl, "Token malformado");
        }
        // PARAMETRO AUTHORIZATION (TOKEN) HA EXPIRADO
        catch (ExpiredJwtException e) {
            httpStatus = HttpStatus.FORBIDDEN;
            handleException(response, httpStatus, e, httpUrl, "El token JWT ha expirado");
        }
        
    }
    
    public void handleException(HttpServletResponse response, HttpStatus httpStatus, Exception e, String httpUrl, String message) throws IOException {
        logger.info("Exception handle: " + e.getClass() + ". URL: " + httpUrl);
        ErrorResponse errorResponse = new ErrorResponse(httpStatus, httpUrl, e.getClass().getSimpleName(), message);
        response.setContentType("application/json");
        response.setStatus(HttpStatus.OK.value());
        response.getWriter().write(convertObjectToJson(errorResponse));
    }
    
    public String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object != null) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        }
        else {
            return null;
        }
    }
    
}