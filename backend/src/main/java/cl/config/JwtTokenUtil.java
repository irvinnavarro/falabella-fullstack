package cl.config;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable {
    
    private static final long serialVersionUID = -2550185165626007488L;
    public static final long JWT_TOKEN_VALIDITY = 1 * 60 * 60;
    @Value("${jwt.secret}")
    private String secret;
    
    // OBTIENE EL NOMBRE DE USUARIO DEL TOKEN
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }
    
    // OBTIENE LA FECHA DE EXPIRACION DEL TOKEN
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }
    
    // OBTIENE VALOR DE CLAVE RECIBIDA DE OBJETO CLAIM
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }
    
    // OBTIENE TODOS LOS VALORES DEL OBJETO CLAIM ASOCIADOS CON EL TOKEN PARA LO CUAL SE REQUIERE LA CLAVE SECRETA
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    // CHEQUEA SI EL TOKEN HA EXPIRADO
    public Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }
    
    // GENERA TOKEN PARA USUARIO
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }
    
    // AL CREAR EL TOKEN
    //1. DEFINE LOS VALORES DEL OBJETO CLAIMS, COMO Issuer, Expiration, Subject Y ID
    //2. FIRMA EL JWT USANDO EL ALGORITMO HS512 Y CLAVE SECRETA.
    //3. SEGUN LA JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    //   COMPACTA EL JWT A UNA CADENA SEGURA PARA URL 
    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
        .signWith(SignatureAlgorithm.HS512, secret).compact();
    }
    
    // VALIDA EL TOKEN
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    
}