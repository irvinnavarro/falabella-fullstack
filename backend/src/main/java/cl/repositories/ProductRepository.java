package cl.repositories;

import cl.models.Product;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    public Optional<Product> findByProdSku(String prodSku);
    
    @Query(value = "SELECT prod_id AS prodId, prod_sku AS prodSku, prod_name AS prodName, prod_brand AS prodBrand, prod_size AS prodSize, "
            + "prod_price AS prodPrice, prod_url AS prodUrl, prod_other_urls AS prodOtherUrls "
            + "FROM t_products",
            nativeQuery = true)
    public List<Map<String, Object>> findAllProducts(Pageable pageable);
    
    @Query(value = "SELECT prod_id AS prodId, prod_sku AS prodSku, prod_name AS prodName, prod_brand AS prodBrand, prod_size AS prodSize, "
            + "prod_price AS prodPrice, prod_url AS prodUrl, prod_other_urls AS prodOtherUrls "
            + "FROM t_products "
            + "WHERE prod_sku LIKE %?1% OR prod_name LIKE %?1% OR prod_brand LIKE %?1% OR prod_size LIKE %?1% OR prod_price LIKE %?1% OR prod_url LIKE %?1%",
            nativeQuery = true)
    public List<Map<String, Object>> findByContains(String filter, Pageable pageable);
    
    @Query(value = "SELECT COUNT(*) "
            + "FROM t_products "
            + "WHERE prod_sku LIKE %?1% OR prod_name LIKE %?1% OR prod_brand LIKE %?1% OR prod_size LIKE %?1% OR prod_price LIKE %?1% OR prod_url LIKE %?1%",
            nativeQuery = true)
    public Long countByContains(String filter);
    
}