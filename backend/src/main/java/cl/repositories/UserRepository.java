package cl.repositories;

import cl.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    public Boolean existsByUserNameAndUserActive(String userName, int userActive);
    
    public Optional<User> findByUserNameAndUserActive(String userName, int userActive);
    
}