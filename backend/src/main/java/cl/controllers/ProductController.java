package cl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cl.models.Product;
import cl.services.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping("products")
@Slf4j
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class ProductController {

    @Autowired
    private ProductService productService; 
    
    /**
     * OBTIENE LISTA DE PRODUCTOS
     *
     * @param request
     * @param requestParams
     * @return
     */
    @GetMapping
    public Map<String, Object> getAll(WebRequest request, @RequestParam Map<String,String> requestParams) {
        // DECLARANDO OBJETOS GENERALES   
        Date currentDate;
        DateFormat datetimeFormat;
        HttpStatus codeMessage;
        Map<String, Object> response;
        Sort sort;
        Pageable pageable;
        List<Map<String, Object>> products;
        // INSTANCIANDO OBJETOS GENERALES
        codeMessage = HttpStatus.OK;
        currentDate = new Date();
        datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        response = new LinkedHashMap<>();
        // DECLARANDO E INICIANDO VARIABLES
        int success = 1;
        Long totalFiltered;
        Long totalRows;
        String datetime = datetimeFormat.format(currentDate);
        String message = "";
        // PARAMETROS DE LA PETICION
        String httpUrl = request.getDescription(false).substring(4);
        int currentPage = (requestParams.get("currentPage")!=null) ? Integer.parseInt(requestParams.get("currentPage")) : 1;
        int perPage = (requestParams.get("perPage")!=null) ? Integer.parseInt(requestParams.get("perPage")) : 10;
        String filter = (requestParams.get("filter")!=null) ? requestParams.get("filter") : "";
        String sortBy = (requestParams.get("sortBy")!=null) ? requestParams.get("sortBy") : "prodId";
        String sortDesc = (requestParams.get("sortDesc")!=null) ? requestParams.get("sortDesc") : "false";
        // DEFINIENDO ORDEN DE DATOS
        sort = Sort.by((sortDesc.equals("false"))?Sort.Direction.ASC:Sort.Direction.DESC, sortBy);
        // DEFINIENDO PAGINACION DE DATOS
        pageable = PageRequest.of(currentPage-1, perPage, sort);
        // OBTENIENDO DATOS Y CANTIDADES
        totalRows = productService.count();
        products = (filter!=null && !filter.equals("")) ? productService.findByContains(filter, pageable) : productService.findAllProducts(pageable);
        totalFiltered = (filter!=null && !filter.equals("")) ? productService.countByContains(filter) : totalRows;
        // SE PREPARA RESPUESTA
        response.put("codeMessage", codeMessage);
        response.put("datetime", datetime);
        response.put("data", products);
        response.put("message", message);
        response.put("status", codeMessage.value());
        response.put("success", success);
        response.put("totalRows",totalRows);
        response.put("totalFiltered",totalFiltered);
        response.put("url", httpUrl);
        // SE ENVIA LA RESPUESTA
        return response;
    }
    
    /**
     * OBTIENE PRODUCTO DADO SU SKU
     *
     * @param request
     * @param prodCode
     * @return
     */
    @GetMapping(path = "/{prodCode}")
    public Map<String, Object> getByProductId(WebRequest request, @PathVariable String prodCode) {
        // DECLARANDO OBJETOS GENERALES   
        Date currentDate;
        DateFormat datetimeFormat;
        HttpStatus codeMessage;
        Map<String, Object> objectResponse;
        Map<String, Object> response;
        // INSTANCIANDO OBJETOS GENERALES
        currentDate = new Date();
        datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        objectResponse = new LinkedHashMap<>();
        response = new LinkedHashMap<>();
        // DECLARANDO OBJETOS DE MODELOS
        Product myProduct;
        Optional<Product> myOpProduct;
        // DECLARANDO E INICIANDO VARIABLES
        int success = 1;
        String datetime = datetimeFormat.format(currentDate);
        String message;
        // PARAMETROS DE LA PETICION
        String httpUrl = request.getDescription(false).substring(4);
        String prodSku = (prodCode != null && !prodCode.equals("")) ? prodCode : null;
        // SE OBTIENE PRODUCTO
        myOpProduct = productService.findByProdSku(prodSku);
        if(myOpProduct.isPresent()) {
            myProduct = myOpProduct.get();
            objectResponse.put("prodSku", myProduct.getProdSku());
            objectResponse.put("prodName", myProduct.getProdName());
            objectResponse.put("prodBrand", myProduct.getProdBrand());
            objectResponse.put("prodSize", myProduct.getProdSize());
            objectResponse.put("prodPrice", myProduct.getProdPrice());
            codeMessage = HttpStatus.OK;
            message = "Consulta exitosa";
        }
        else {
            codeMessage = HttpStatus.NOT_FOUND;
            message = String.format("Producto con SKU %s no encontrado.", prodSku);
        }
        // SE PREPARA RESPUESTA
        response.put("codeMessage", codeMessage);
        response.put("datetime", datetime);
        response.put("data", objectResponse);
        response.put("message", message);
        response.put("status", codeMessage.value());
        response.put("success", success);
        response.put("url", httpUrl);
        // SE ENVIA RESPUESTA
        return response;
    }
    
    /**
     * CREA UN NUEVO PRODUCTO
     *
     * @param request
     * @param json
     * @return
     */
    @PostMapping(path = "/", consumes = "application/json")
    public Map<String, Object> create(WebRequest request, @Valid @RequestBody Map json) {
        // DECLARANDO OBJETOS GENERALES   
        Date currentDate;
        DateFormat datetimeFormat;
        HttpStatus codeMessage;
        Map<String, Object> objectResponse;
        Map<String, Object> response;
        // INSTANCIANDO OBJETOS GENERALES
        currentDate = new Date();
        datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        objectResponse = new LinkedHashMap<>();
        response = new LinkedHashMap<>();
        // DECLARANDO OBJETOS DE MODELOS
        Product myProduct;
        Optional<Product> myOpProduct;
        // DECLARANDO E INICIANDO VARIABLES
        int success = 1;
        String datetime = datetimeFormat.format(currentDate);
        String message = "";
        // PARAMETROS DE LA PETICION
        String httpUrl = request.getDescription(false).substring(4);
        String prodSku = (json.get("prodSku") != null&& !json.get("prodSku").equals("")) ? String.valueOf(json.get("prodSku")) : null;
        String prodName = (json.get("prodName") != null && !json.get("prodName").equals("")) ? String.valueOf(json.get("prodName")) : null;
        String prodBrand = (json.get("prodBrand") != null&& !json.get("prodBrand").equals("")) ? String.valueOf(json.get("prodBrand")) : null;
        String prodSize = (json.get("prodSize") != null&& !json.get("prodSize").equals("")) ? String.valueOf(json.get("prodSize")) : null;
        String prodPrice = (json.get("prodPrice") != null&& !json.get("prodPrice").equals("")) ? String.valueOf(json.get("prodPrice")) : null;
        String prodUrl = (json.get("prodUrl") != null&& !json.get("prodUrl").equals("")) ? String.valueOf(json.get("prodUrl")) : null;
        String prodOtherUrls = (json.get("prodOtherUrls") != null&& !json.get("prodOtherUrls").equals("")) ? String.valueOf(json.get("prodOtherUrls")) : null;
        // VALIDACIONES
        if(prodSku == null){
            message = String.format("SKU es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("FAL-[1-9][0-9]{6,7}", prodSku)){
            message = String.format("SKU permitidos desde FAL-1000000 hasta FAL-99999999");
        }
        else if(prodName == null){
            message = String.format("Nombre de producto es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\\s]{3,50}", prodName)){
            message = String.format("Nombre de producto debe contener solo letras y/o números y una longitud de entre 3 y 50 caracteres");
        }
        else if(prodBrand == null){
            message = String.format("Marca de producto  es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\\s]{3,50}", prodBrand)){
            message = String.format("Marca de producto debe contener solo letras y/o números y una longitud de entre 3 y 50 caracteres");
        }
        else if(prodSize == null){
            message = String.format("Talla de producto es requerida y/o no puede estar vacía");
        }
        else if(prodPrice == null){
            message = String.format("Precio de producto es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("[1-9]([0-9]{1,7})?[\\.][0-9]{2}", prodPrice)){
            message = String.format("Precio de producto permitidos desde 1.00 hasta 99999999.00");
        }
        else if(prodUrl == null){
            message = String.format("Url de producto es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)", prodUrl)){
            message = String.format("Url de Imagen debe ser una url válida");
        }
        else if(prodOtherUrls != null && !isProdOtherUrlsValid(prodOtherUrls)){
            message = String.format("Otras URLs no tienen el formato requerido");
        }
        else {
            myOpProduct = productService.findByProdSku(prodSku);
            if (myOpProduct.isPresent()) {
                message = String.format("SKU '%s', ya existe asociado a otro producto", prodSku);
            }
        }
        // SE CUMPLEN LAS VALIDACIONES
        if(message.equals("")){
            // SE CREA PRODUCTO
            myProduct = new Product();
            myProduct.setProdSku(prodSku);
            myProduct.setProdName(prodName);
            myProduct.setProdBrand(prodBrand);
            myProduct.setProdSize(prodSize);
            myProduct.setProdPrice(Double.parseDouble(prodPrice));
            myProduct.setProdUrl(prodUrl);
            myProduct.setProdOtherUrls(prodOtherUrls);
            myProduct = productService.save(myProduct);
            // SE DEFINEN LOS VALORES DE LA RESPUESTA
            objectResponse.put("prodId",myProduct.getProdId());
            objectResponse.put("prodSku",myProduct.getProdSku());
            objectResponse.put("prodName",myProduct.getProdName());
            objectResponse.put("prodBrand",myProduct.getProdBrand());
            objectResponse.put("prodSize",myProduct.getProdSize());
            objectResponse.put("prodPrice",myProduct.getProdPrice());
            objectResponse.put("prodUrl",myProduct.getProdUrl());
            objectResponse.put("prodOtherUrls",myProduct.getProdOtherUrls());
            codeMessage = HttpStatus.OK;
            message = "Registro exitoso";
        }
        // NO SE CUMPLEN LAS VALIDACIONES
        else {
            codeMessage = HttpStatus.CONFLICT;
        }
        // SE PREPARA RESPUESTA
        response.put("codeMessage", codeMessage);
        response.put("datetime", datetime);
        response.put("data", objectResponse);
        response.put("message", message);
        response.put("status", codeMessage.value());
        response.put("success", success);
        response.put("url", httpUrl);
        // SE ENVIA RESPUESTA
        return response;
    }
    
    /**
     * EDITA UN PRODUCTO EXISTENTE
     *
     * @param request
     * @param json
     * @param prodCode
     * @return
     */
    @PutMapping(path = "/{prodCode}", consumes = "application/json")
    public Map<String, Object> update(WebRequest request, @Valid @RequestBody Map json, @PathVariable String prodCode) {
        // DECLARANDO OBJETOS GENERALES   
        Date currentDate;
        DateFormat datetimeFormat;
        HttpStatus codeMessage;
        Map<String, Object> objectResponse;
        Map<String, Object> response;
        // INSTANCIANDO OBJETOS GENERALES
        currentDate = new Date();
        datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        objectResponse = new LinkedHashMap<>();
        response = new LinkedHashMap<>();
        // DECLARANDO OBJETOS DE MODELOS
        Product myProduct;
        Optional<Product> myOpProduct = null;
        // DECLARANDO E INICIANDO VARIABLES
        int success = 1;
        String datetime = datetimeFormat.format(currentDate);
        String message = "";
        // PARAMETROS DE LA PETICION
        String httpUrl = request.getDescription(false).substring(4);
        String prodSku = (prodCode != null && !prodCode.equals("")) ? prodCode : null;
        String prodName = (json.get("prodName") != null && !json.get("prodName").equals("")) ? String.valueOf(json.get("prodName")) : null;
        String prodBrand = (json.get("prodBrand") != null&& !json.get("prodBrand").equals("")) ? String.valueOf(json.get("prodBrand")) : null;
        String prodSize = (json.get("prodSize") != null&& !json.get("prodSize").equals("")) ? String.valueOf(json.get("prodSize")) : null;
        String prodPrice = (json.get("prodPrice") != null&& !json.get("prodPrice").equals("")) ? String.valueOf(json.get("prodPrice")) : null;
        String prodUrl = (json.get("prodUrl") != null&& !json.get("prodUrl").equals("")) ? String.valueOf(json.get("prodUrl")) : null;
        String prodOtherUrls = (json.get("prodOtherUrls") != null&& !json.get("prodOtherUrls").equals("")) ? String.valueOf(json.get("prodOtherUrls")) : null;
        // VALIDACIONES
        if(prodSku == null){
            message = String.format("SKU es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("FAL-[1-9][0-9]{6,7}", prodSku)){
            message = String.format("SKU permitidos desde FAL-1000000 hasta FAL-99999999");
        }
        else if(prodName == null){
            message = String.format("Nombre de producto es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\\s]{3,50}", prodName)){
            message = String.format("Nombre de producto debe contener solo letras y/o números y una longitud de entre 3 y 50 caracteres");
        }
        else if(prodBrand == null){
            message = String.format("Marca de producto  es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\\s]{3,50}", prodBrand)){
            message = String.format("Marca de producto debe contener solo letras y/o números y una longitud de entre 3 y 50 caracteres");
        }
        else if(prodSize == null){
            message = String.format("Talla de producto es requerida y/o no puede estar vacía");
        }
        else if(prodPrice == null){
            message = String.format("Precio de producto es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("[1-9]([0-9]{1,7})?[\\.][0-9]{2}", prodPrice)){
            message = String.format("Precio de producto permitidos desde 1.00 hasta 99999999.00");
        }
        else if(prodUrl == null){
            message = String.format("Url de producto es requerido y/o no puede estar vacío");
        }
        else if(!Pattern.matches("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)", prodUrl)){
            message = String.format("Url de Imagen debe ser una url válida");
        }
        else if(prodOtherUrls != null && !isProdOtherUrlsValid(prodOtherUrls)){
            message = String.format("Otras URLs no tienen el formato requerido");
        }
        else {
            myOpProduct = productService.findByProdSku(prodSku);
            if (myOpProduct.isEmpty()) {
                message = String.format("SKU '%s', no encontrado", prodSku);
            }
        }
        // SE CUMPLEN LAS VALIDACIONES
        if(message.equals("")){
            // SE EDITA PRODUCTO
            myProduct = myOpProduct.get();
            myProduct.setProdSku(prodSku);
            myProduct.setProdName(prodName);
            myProduct.setProdBrand(prodBrand);
            myProduct.setProdSize(prodSize);
            myProduct.setProdPrice(Double.parseDouble(prodPrice));
            myProduct.setProdUrl(prodUrl);
            myProduct.setProdOtherUrls(prodOtherUrls);
            myProduct = productService.save(myProduct);
            // SE DEFINEN LOS VALES DE LA RESPUESTA
            objectResponse.put("prodId",myProduct.getProdId());
            objectResponse.put("prodSku",myProduct.getProdSku());
            objectResponse.put("prodName",myProduct.getProdName());
            objectResponse.put("prodBrand",myProduct.getProdBrand());
            objectResponse.put("prodSize",myProduct.getProdSize());
            objectResponse.put("prodPrice",myProduct.getProdPrice());
            objectResponse.put("prodUrl",myProduct.getProdUrl());
            objectResponse.put("prodOtherUrls",myProduct.getProdOtherUrls());
            codeMessage = HttpStatus.OK;
            message = "Actualización exitosa";
        }
        else {
            codeMessage = HttpStatus.CONFLICT;
        }
        // SE PREPARA RESPUESTA
        response.put("codeMessage", codeMessage);
        response.put("datetime", datetime);
        response.put("data", objectResponse);
        response.put("message", message);
        response.put("status", codeMessage.value());
        response.put("success", success);
        response.put("url", httpUrl);
        // SE ENVIA RESPUESTA
        return response;
    }
    
    /**
     * ELIMINA UN PRODUCTO EXISTENTE
     *
     * @param request
     * @param prodCode
     * @return
     */
    @DeleteMapping(path = "/{prodCode}")
    public Map<String, Object> delete(WebRequest request, @PathVariable String prodCode) {
        // DECLARANDO OBJETOS GENERALES   
        Date currentDate;
        DateFormat datetimeFormat;
        HttpStatus codeMessage;
        Map<String, Object> objectResponse;
        Map<String, Object> response;
        // INSTANCIANDO OBJETOS GENERALES
        currentDate = new Date();
        datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        objectResponse = new LinkedHashMap<>();
        response = new LinkedHashMap<>();
        // DECLARANDO OBJETOS DE MODELOS
        Product myProduct;
        Optional<Product> myOpProduct;
        // DECLARANDO E INICIANDO VARIABLES
        int success = 1;
        String datetime = datetimeFormat.format(currentDate);
        String message = "";
        // PARAMETROS DE LA PETICION
        String httpUrl = request.getDescription(false).substring(4);
        String prodSku = (prodCode != null && !prodCode.equals("")) ? prodCode : null;
        // VALIDACIONES
        myOpProduct = productService.findByProdSku(prodSku);
        if (myOpProduct.isEmpty()) {
            message = String.format("SKU '%s', no encontrado", prodSku);
        }
        // SE CUMPLEN LAS VALIDACIONES
        if(message.equals("")){
            // SE EDITA PRODUCTO
            myProduct = myOpProduct.get();
            productService.deleteByProdId(myProduct.getProdId());
            // SE DEFINEN LOS VALES DE LA RESPUESTA
            objectResponse.put("prodId",myProduct.getProdId());
            objectResponse.put("prodSku",myProduct.getProdSku());
            objectResponse.put("prodName",myProduct.getProdName());
            objectResponse.put("prodBrand",myProduct.getProdBrand());
            objectResponse.put("prodSize",myProduct.getProdSize());
            objectResponse.put("prodPrice",myProduct.getProdPrice());
            codeMessage = HttpStatus.OK;
            message = "Eliminación exitosa";
        }
        else {
            codeMessage = HttpStatus.CONFLICT;
        }
        // SE PREPARA RESPUESTA
        response.put("codeMessage", codeMessage);
        response.put("datetime", datetime);
        response.put("data", objectResponse);
        response.put("message", message);
        response.put("status", codeMessage.value());
        response.put("success", success);
        response.put("url", httpUrl);
        // SE ENVIA RESPUESTA
        return response;
    }
    
    public static boolean isProdOtherUrlsValid(String jsonInString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(jsonInString);
            JSONParser jsonParse = new JSONParser();
            JSONArray jsonArray = (JSONArray) jsonParse.parse(jsonInString);
            for (int i = 0; i < jsonArray.size(); i++) {
                if(!Pattern.matches("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)", (String) jsonArray.get(i))){
                    return false;
                }
            }
           return true;
        }
        catch (IOException | ParseException e) {
           return false;
        }
    }
    
}