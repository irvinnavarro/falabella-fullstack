package cl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cl.config.JwtTokenUtil;
import cl.services.UserService;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping("")
@Slf4j
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class SessionController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserService userService; 
    
    /**
     * OBTIENE TOKEN DE USUARIO
     *
     * @param request
     * @param requestBody
     * @return
     */
    @PostMapping(path = "/authenticate", consumes = "application/json")
    public Map<String, Object> createAuthenticationToken(WebRequest request, @Valid @RequestBody Map requestBody) {
        // DECLARANDO OBJETOS GENERALES
        Map<String, Object> response;
        UserDetails userDetails;
        // INSTANCIANDO OBJETOS GENERALES
        response = new LinkedHashMap<>();
        // PARAMETROS DE LA PETICION
        String userName = (requestBody.get("username") != null) ? (String) requestBody.get("username") : "";
        String userPassword = (requestBody.get("password") != null) ? (String) requestBody.get("password") : "";
        // DECLARANDO E INICIANDO VARIABLES
        String token = null;
        // SE ENVIA CREDENCIAL AL ADMINISTRADOR DE AUTENTICACION
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, userPassword));
        // SI EL USUARIO ESTA ACTIVO
        if (userService.existsByUserNameAndUserActive(userName,1)){
            userDetails = userService.loadUserByUsername(userName);
            token = jwtTokenUtil.generateToken(userDetails);
        }
        // SE PREPARA LA RESPUESTA
        response.put("token",token);
        // SE ENVIA LA RESPUESTA
        return response;
    }
    
}