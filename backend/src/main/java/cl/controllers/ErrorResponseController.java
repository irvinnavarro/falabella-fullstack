
package cl.controllers;

import cl.models.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice(annotations=RestController.class)
public class ErrorResponseController {
 
    private final Logger logger = LoggerFactory.getLogger(ErrorResponseController.class);
    
    @ExceptionHandler({BadCredentialsException.class, InternalAuthenticationServiceException.class})
    public ErrorResponse handleBadCredentialsException(Exception e, WebRequest request) {
        HttpStatus httpStatus = HttpStatus.FORBIDDEN;
        String httpUrl = request.getDescription(false).substring(4);
        String message = "";
        if(httpUrl.equals("/authenticate")) {
            message = "Usuario y/o contraseña incorrecta.";
        }
        return handleException(e, httpStatus, httpUrl, message);
    }
        
    public ErrorResponse handleException(Exception e, HttpStatus httpStatus, String httpUrl, String message) {
        logger.info("Exception handle: " + e.getClass() + ". URL: " + httpUrl);
        return new ErrorResponse(httpStatus, httpUrl, e.getClass().getSimpleName(), message);
    }

}
