package cl.services;

import cl.models.Product;
import cl.repositories.ProductRepository;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    
    private final ProductRepository productRepository;

    public long count() {
        return productRepository.count();
    }
    
    public Optional<Product> findByProdId(Long prodId) {
        return productRepository.findById(prodId);
    }
    
    public Optional<Product> findByProdSku(String prodSku) {
        return productRepository.findByProdSku(prodSku);
    }
    
    public List<Map<String, Object>> findAllProducts(Pageable pageable) {
        return productRepository.findAllProducts(pageable);
    }
    
    public List<Map<String, Object>> findByContains(String filter, Pageable pageable) {
        return productRepository.findByContains(filter, pageable);
    }
    
    public Long countByContains(String filter) {
        return productRepository.countByContains(filter);
    }
    
    public Product save(Product product) {
        return productRepository.save(product);
    }
    
    public void deleteByProdId(Long prodId) {
        productRepository.deleteById(prodId);
    }

}