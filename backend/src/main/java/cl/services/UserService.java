package cl.services;

import cl.models.User;
import cl.repositories.UserRepository;
import java.util.HashSet;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService{
    
    private final UserRepository userRepository;
    
    public boolean existsByUserNameAndUserActive(String userName, int userActive) {
        return userRepository.existsByUserNameAndUserActive(userName, userActive);
    }
    
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
        User user = userRepository.findByUserNameAndUserActive(userName, 1).get();
        if (user == null) {
            throw new UsernameNotFoundException("Usuario no encontrado con parametro username: " + userName);
        }
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getUserPass(), grantedAuthorities);
    }

}