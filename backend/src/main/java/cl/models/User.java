package cl.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "m_users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", columnDefinition = "int(11) UNSIGNED NOT NULL")
    private Long userId;
    @Column(name = "user_name", columnDefinition = "varchar(20) NOT NULL")
    @NotBlank(message = "Nombre obligatorio")
    @Size(min = 5, max = 20, message = "Nombre entre 5 y 20 caracteres")
    private String userName;
    @Column(name = "user_pass", columnDefinition = "varchar(60) NOT NULL")
    @NotBlank(message = "Password obligatorio")
    private String userPass;
    @Column(name = "user_active", columnDefinition = "tinyint(1) UNSIGNED NOT NULL DEFAULT 1")
    private Integer userActive = 1;
    
    public User(){
        super();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }
    
    public Integer getUserActive() {
        return userActive;
    }

    public void setUserActive(Integer userActive) {
        this.userActive = userActive;
    }
    
}