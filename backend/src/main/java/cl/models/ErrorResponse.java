package cl.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpStatus;

public class ErrorResponse {

    private HttpStatus codeMessage;
    private String exception;
    private List<String> messages = new ArrayList();
    private int status;
    private int success;
    private String timestamp;
    private String url;

    public ErrorResponse(HttpStatus httpStatus, String url, String exception, String message) {
        Date currentDate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.codeMessage = httpStatus;
        this.exception = exception;
        this.messages.add(message);
        this.status = httpStatus.value();
        this.success = 0;
        this.timestamp = dateFormat.format(currentDate);
        this.url = url;
    }

    public HttpStatus getCodeMessage() {
        return codeMessage;
    }

    public void setCodeMessage(HttpStatus codeMessage) {
        this.codeMessage = codeMessage;
    }
    
    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
    
    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}