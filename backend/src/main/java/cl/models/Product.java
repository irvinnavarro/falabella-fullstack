package cl.models;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "t_products")
public class Product implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "prod_id", columnDefinition = "int(11) UNSIGNED NOT NULL")
    private Long prodId;
    @Column(name = "prod_sku", columnDefinition = "varchar(12) NOT NULL")
    @NotBlank(message = "Sku obligatorio")
    @Size(min = 11, max = 12, message = "Sku entre 11 y 12 caracteres")
    private String prodSku;
    @Column(name = "prod_name", columnDefinition = "varchar(50) NOT NULL")
    @NotBlank(message = "Nombre obligatorio")
    @Size(min = 3, max = 50, message = "BRAND entre 3 y 50 caracteres")
    private String prodName;
    @Column(name = "prod_brand", columnDefinition = "varchar(50) NOT NULL")
    @NotBlank(message = "Marca obligatoria")
    @Size(min = 3, max = 50, message = "BRAND entre 3 y 50 caracteres")
    private String prodBrand;
    @Column(name = "prod_size", columnDefinition = "varchar(255) NOT NULL")
    @NotBlank(message = "Talla obligatoria")
    private String prodSize;
    @Column(name = "prod_price", columnDefinition = "double NOT NULL")
    @NotBlank(message = "Precio obligatorio")
    @DecimalMin(value = "1.00", message = "Your message...")
    @DecimalMax(value = "99999999.00", message = "Your message...")
    private Double prodPrice;
    @Column(name = "prod_url", columnDefinition = "text NOT NULL")
    @NotBlank(message = "Imagen principal obligatoria")
    private String prodUrl;
    @Column(name = "prod_other_urls", columnDefinition = "text")
    private String prodOtherUrls;
    
    public Product(){
        super();
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public String getProdSku() {
        return prodSku;
    }

    public void setProdSku(String prodSku) {
        this.prodSku = prodSku;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdBrand() {
        return prodBrand;
    }

    public void setProdBrand(String prodBrand) {
        this.prodBrand = prodBrand;
    }

    public String getProdSize() {
        return prodSize;
    }

    public void setProdSize(String prodSize) {
        this.prodSize = prodSize;
    }

    public Double getProdPrice() {
        return prodPrice;
    }

    public void setProdPrice(Double prodPrice) {
        this.prodPrice = prodPrice;
    }

    public String getProdUrl() {
        return prodUrl;
    }

    public void setProdUrl(String prodUrl) {
        this.prodUrl = prodUrl;
    }
    
    public String getProdOtherUrls() {
        return prodOtherUrls;
    }
    
    public void setProdOtherUrls(String prodOtherUrls) {
        this.prodOtherUrls = prodOtherUrls;
    }
    
}