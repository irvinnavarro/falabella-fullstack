import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

Axios.defaults.headers.common['Content-Type'] = 'application/json'
if (store.getters.token) {
  Axios.defaults.headers.common['Authorization'] = store.getters.token
}

Vue.prototype.$apiUrl = 'http://localhost:8081/'
Vue.prototype.$http = Axios

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
