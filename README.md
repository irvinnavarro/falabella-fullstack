Proyecto fullstack

## Backend

Tecnologías

Server Tomcat 9.0.37
JDK 13
Springboot 2.5.2
MySQL 5.7
IDE Apache Netbeans 11.1

Despliegue

El archivo de configuración se encuentra en la ruta ./ejercicio1/src/main/resources/application.properties, donde se debe configurar 
el esquema y credenciales de Base de datos.

Para el despliegue local se utiliza el IDE Apache Netbeans 11.1 con el server Server Tomcat 9.0.37 configurado. 

Para el despliegue en producción se debe construir el archivo .war y se sube al servidor.

## Frontend

Tecnologías

VueJs 2.6.10
Bootstrap 4.5.3

Despliegue

El archivo de configuración se encuentra en la ruta ./ejercicio3/src/main.js, donde se debe configurar el endpoint del Backend. 

Para realizar la compilación se debe tener instalado npm.

Para compilar y desplegar en ambientes de desarrollo se debe ejeuctar el comando

npm run serve

Para compilar y desplegar en ambientes de producción se debe ejeuctar el siguiente comando que generará la carpeta dist en la ruta ./ejercicio3/

npm run build

Se cambia el nombre de la carpeta "dist" por "front" y se sube al servidor

Para acceder a la aplicación front a través de un navegador se ingresa a la siguiente URL sustituyendo 
el nombre de dominio y puerto donde esta la aplicación.

http://nombredominio:puerto/front
